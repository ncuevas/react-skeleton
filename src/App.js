import React, { Component } from 'react'
import './App.css';

import Header from './Header';
import Footer from './Footer';
import Card from './Card';

class App extends Component {

 constructor(props) {
   super(props);
   this.state = {
     items: []
   };
 }

/*

<input type="button"
        value="boton"
        onClick={event => this.onClick(event)} />
onClick (event) {
    console.log(event.target.value)
}

*/


componentDidMount () {
  fetch('http://localhost/dam/php/ecommerce_ejemplo_1/producto.php')
  .then(response => response.json())
  .then(data => {
    console.log(data);
    this.setState({
      items: data.response
    });
  })
  .catch(err => {
    console.log(err);
  })
}


 render() {
   return (
   <div className="container-fluid">
     <Header></Header>
     <div className="row">
      <div className="col-4">col-4</div>
      <div className="col-8">
        {this.state.items.map(
            (item,index) =>
             <Card></Card>
            )
        }
      </div>
    </div>
     <Footer></Footer>
   </div>
   );
 }
}

export default App;
